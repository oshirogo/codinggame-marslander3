#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

const int LANDING_MIN_LEN = 1000;
const int LANDING_MARGIN = 50; //margin of error for landing
const int PEAK_CLEAR = 300; //ship must be at least this much above a peak to avoid collision
const int LOCK_ROTATE_Y = 1400; //at this height, make sure ship is straight up
const int MAX_ALT = 2700;
const int CLOSE_DISTANCE = 2000;
const int SPEED_FAR = 30;
const int SPEED_CLOSE = 20;
const float PI = 3.14159265;

struct input
{
    int X;
    int Y;
    int hSpeed; // the horizontal speed (in m/s), can be negative.
    int vSpeed; // the vertical speed (in m/s), can be negative.
    int fuel; // the quantity of remaining fuel in liters.
    int rotate; // the rotation angle in degrees (-90 to 90).
    int power; // the thrust power (0 to 4).
};

struct land
{
    pair<int, int> finalLand;
    pair<int, int> nextLand;
};

template <typename T>
constexpr T clamp(const T& n, const T& lower, const T& upper) {
    return max(lower, min(n, upper));
}

int getSurfaceN()
{
#ifdef editor
    return 22;
#else
    int ret = 0;
    cin >> ret; cin.ignore();
    return ret;
#endif
}

vector<pair<int, int>> getPoints(int surfaceN)
{
#ifdef editor
     //TEST 1
     vector<pair<int, int>> points = {
     {0,450},
     {300,750},
     {1000,450},
     {1500,650},
     {1800,850},
     {2000,1950},
     {2200,1850},
     {2400,2000},
     {3100,1800},
     {3150,1550},
     {2500,1600},
     {2200,1550},
     {2100,750},
     {2200,150},
     {3200,150},
     {3500,450},
     {4000,950},
     {4500,1450},
     {5000,1550},
     {5500,1500},
     {6000,950},
     {6999,1750}
     };
    /*
    //TEST 2
    vector<pair<int, int>> points = {
        {4700,220},
        {4750,1000},
        {4700,1650},
        {4000,1700},
        {3700,1600},
        {3750,1900},
        {4000,2100},
        {4900,2050},
        {5100,1000},
        {5500,500},
        {6200,800},
        {0,1800},
        {300,1200},
        {1000,1550},
        {2000,1200},
        {2500,1650},
        {3700,220},
        {4700,220},
        {4750,1000},
        {4700,1650},
        {4000,1700},
        {3700,1600},
        {3750,1900},
        {4000,2100},
        {4900,2050},
        {5100,1000},
        {5500,500},
        {6200,800}
    };
     */
#else
    vector<pair<int, int>> points = vector<pair<int, int>>(surfaceN);
    for (int i = 0; i < surfaceN; i++) {
        int landX; // X coordinate of a surface point. (0 to 6999)
        int landY; // Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
        points[i] = make_pair(landX, landY);
        cin >> landX >> landY ; cin.ignore();
    }
#endif
    return points;
}


land getLanding(vector<pair<int, int>> points, int shipX, int shipY)
{
    land ret;
    pair<int, int> nextLand;
    pair<int, int> finalLand;
    auto surfaceN = points.size();
    int landIndex = 0;
    int finalLandIndex = 0;
    //find the landing area
    for(auto i=1; i<surfaceN; ++i)
    {
        if(points[i].second == points[i-1].second && (points[i].first - points[i-1].first) >= LANDING_MIN_LEN)
        {
            finalLandIndex = i;
        }
    }
    landIndex = finalLandIndex;
    
    //find the x position that is closest to landing center without anything above it
    auto blocked = true;
    //move landing spot closer to ship until it isn't blocked anymore
    auto incr = (shipX > nextLand.first) ? 1 : -1;
    nextLand = finalLand = make_pair((points[landIndex].first + points[landIndex-1].first) / 2, points[landIndex].second);
    bool inCave = false;
    int numTries = 0;
    while(blocked)
    {
        blocked = false;
        for(auto i=1; i<surfaceN; ++i)
        {
            if(
               i != landIndex &&
               points[i].first >= nextLand.first &&
               points[i-1].first <= nextLand.first &&
               points[i-1].second > nextLand.second &&
               points[i].second > nextLand.second &&
               points[i-1].second < shipY &&
               points[i].second < shipY
               )
            {
                cerr << "blocking" << endl;
                cerr << "   landing " << nextLand.first << " " << nextLand.second << endl;
                cerr << "   block 1 " << points[i-1].first << " " << points[i-1].second << endl;
                cerr << "   block 2 " << points[i].first << " " << points[i].second << endl;
                blocked = true;
                landIndex += incr;
                numTries++;
                //FIXME better way to see if in cave
                if(!inCave && numTries > 4)
                {
                    //in a cave, try finding a spot in the opposite direction
                    cerr << "In a cave!  Trying opposite direction." << endl;
                    incr = 0 - incr;
                    inCave = true;
                    landIndex = finalLandIndex;
                    i = 1;
                    
                }
                
                nextLand = (landIndex - finalLandIndex > 1) ? points[landIndex-1] : nextLand = points[landIndex];;
                
                
                //nextLand = make_pair((points[landIndex].first + points[landIndex-1].first) / 2, points[landIndex].second);
            }
        }
    }
    
    cerr << "Ship X:          " << shipX << endl;
    cerr << "Final land spot: " << finalLand.first << " " << finalLand.second << endl;
    cerr << "Next land spot:  " << nextLand.first << " " << nextLand.second << endl;
    ret.nextLand = nextLand;
    ret.finalLand = finalLand;
    return ret;
}

input getInput()
{
    input ret;
#ifdef editor
    //X=3602m, Y=1729m, HSpeed=-129m/s VSpeed=-40m/s
    //Fuel=846l, Angle=45°, Power=4 (4.0m/s2)
    ret.X = 3602;
    ret.Y = 1729;
    ret.hSpeed = -129;
    ret.vSpeed = -40;
    ret.fuel = 8461;
    ret.rotate = -45;
    ret.power = 4;
#else
    cin >> ret.X >> ret.Y >> ret.hSpeed >> ret.vSpeed >> ret.fuel >> ret.rotate >> ret.power; cin.ignore();
#endif
    
    return ret;
        
}

void outClose(input input, pair<int, int> landingSpot)
{
    int newPower = 2;
    int newRotate = 0;
    //if too low, don't rotate ship
    if(input.Y < LOCK_ROTATE_Y && input.vSpeed < -20) newPower = 4;
    //if moving too fast horizontally, slow down
    if(input.hSpeed < -20) newRotate = -45;
    if(input.hSpeed > 20) newRotate = 45;
    
    cout << newRotate << " " << newPower << endl;
    
}

void outFar(input input, pair<int, int> landingSpot, bool isFinalLand)
{
    float distance = abs(input.X - landingSpot.first);
    
    int newPower = 4;
    int newRotate = 0;
    
    //how fast do we want to go?  Farther away go faster to save fuel, closer go slower
    float vx = (distance > CLOSE_DISTANCE) ? SPEED_FAR : SPEED_CLOSE;
    
    //adjust for the ship's current speed
    if(landingSpot.first < input.X)
    {
        vx += input.hSpeed;
    }
    else
    {
        vx -= input.hSpeed;
    }
    
    //how much power is needed for horizontal velocity?
    auto xNormal = (vx > newPower) ? 1.0 : vx / newPower;
    
    //get angle required to generate horizontal power
    //from pythagrean, x^2 + y^2 = 1, so y = sqrt(1 - x^2)
    auto yNormal = sqrt(1 - (xNormal * xNormal));
    //tan(Θ) = y / x
    auto angleRad = atan(yNormal / xNormal);
    auto direction = (landingSpot.first > input.X) ? -1 : 1;
    //auto direction = 1;
    //angle in degrees, rotated 90 degrees
    auto clampAmt = 45.0;
    auto unclampedAngle = (angleRad * 180 / PI) + 90 * direction;
    auto angle = clamp(unclampedAngle, 0 - clampAmt, clampAmt);
    newRotate = (int) angle;
    
    cerr << "Distance: " << distance << " meters" << endl;
    
    cout << newRotate << " " << newPower << endl;
}

bool isTooLow(input input, land landing, bool isFinalLand, vector<pair<int, int>> points)
{
    //it's ok to be low when we're trying to land
    if(isFinalLand) return false;
    
    //check nearby land to see if we're too close to it
    auto surfaceN = points.size();
    for(auto i=1; i<surfaceN; ++i)
    {
        if((points[i].first >= input.X && points[i-1].first <= input.X) || (points[i].first <= input.X && points[i-1].first >= input.X))
        {
            if(input.Y < points[i].second + PEAK_CLEAR) return true;
            if(input.Y < points[i-1].second + PEAK_CLEAR) return true;
        }
    }
    return false;
}

int main()
{
    auto surfaceN = getSurfaceN(); // the number of points used to draw the surface of Mars.
    auto points = getPoints(surfaceN);
    land landing;
    auto isArrived = false;
    bool isFinalLand = false;
    
    // game loop
    while (1) {
        auto input = getInput();
        if(landing.nextLand.first == 0 || abs(input.X - landing.nextLand.first) < LANDING_MARGIN)
        {
            //caclulate the landing spot.  If we've arrived, calculate again
            //for first calculation of landing, use a high ship altitude to make sure we find a good spot
            auto shipY = isArrived ? input.Y : MAX_ALT;
            landing = getLanding(points, input.X, shipY);
            isFinalLand = landing.nextLand.first == landing.finalLand.first;
        }
        
        if(abs(input.X - landing.nextLand.first) < LANDING_MARGIN || input.Y < LOCK_ROTATE_Y)
        {
            //logic when close to landing spot
            outClose(input, landing.nextLand);
            
        }
        else if(isTooLow(input, landing, isFinalLand, points))
        {
            //logic when ship is too low and may crash
            cout << "0 4" << endl;
        }
        else
        {
            //logic when not close to landing spot
            outFar(input, landing.nextLand, isFinalLand);
        }
        
#ifdef editor
        return 0;
#endif
    }
}
